#!/bin/bash
cp *.sh /etc/profile.d/
# This is only for Dell Inspireon 3168
#cp 90-custom-keyboard.hwdb /etc/udev/hwdb.d/
# If you don't want user configs in /etc/skel/, specify a home directory:
homedir=
if [ -z $homedir ]
then
	homedir='/etc/skel/'
elif [[ ! $homedir =~ ^(.)*\/$ ]]
then
	homedir+='/'
fi
mkdir ${homedir}.config
cp -rv {i3,gtk-3.0,dunst,termite} ${homedir}.config/
cp -v ${homedir}.config/i3/time /usr/libexec/i3blocks/
cp -v .gtkrc-2.0 ${homedir}
chown -Rv $(stat -c '%u:%g' ${homedir}) ${homedir}/.config
# This is to override DejaVu fonts in Fedora with google-noto fonts
for file in fonts.conf.d/* ;
do 
	cp -v $file /usr/share/fontconfig/conf.avail/
	ln -sv /usr/share/fontconfig/conf.avail/${file} /etc/fonts/conf.d/${file}
done
# Pacman hooks for ache cleanup, require pacman-contrib package
cp -v pacman.d/hooks/* /etc/pacman.d/hooks/ && echo "Please install 'pacman-contrib' package for these pacman hooks to work."
