#!/usr/bin/env python3
# Define output fields. key is field name value is color for pango markup
load_fields = {
        'user':'#0ED839',
        'nice':'#DDDD13',
        'system':'#E92F2F',
        'iowait':'black',
        #'idle':'#3B48E3'
        }
# This is a fallback function for the case psutil library is not available
def cpu_load_mpstat(display_fields):
    from os import popen
    from sys import exit
    from shutil import which
    import json
    if which('mpstat') is None: exit("'psutil' python library and fallback 'mpstat' executable not found.\nPlease install either python3-psutil or sysstat package")
    cpu_load_out = {}
    cpu_load = json.loads(popen('mpstat -o JSON 1 1').read())['sysstat']['hosts'][0]['statistics'][0]['cpu-load'][0]
    cpu_load['user'] = cpu_load.pop('usr')
    cpu_load['system'] = cpu_load.pop('sys')
    for field in display_fields:
        cpu_load_out[field] = cpu_load[field]
    cpu_load_out['idle'] = cpu_load['idle']
    return cpu_load_out
# This function uses psutil library to fetch CPU stats
def cpu_load_psutil(display_fields):
    cpu_load_out = {}
    try:
        from psutil import cpu_times_percent
    except ImportError:
        # Fall back to mpstat executable if no psutil library
        cpu_load_out = cpu_load_mpstat(display_fields)
        return cpu_load_out
    cpu_load = cpu_times_percent(1)
    for field in display_fields:
        cpu_load_out[field] = getattr(cpu_load,field)
    cpu_load_out['idle'] = getattr(cpu_load,'idle')
    return cpu_load_out

cpu_load_out = cpu_load_psutil(load_fields)
for field in load_fields:
    print("<span color='",load_fields[field],"'>",str(round(cpu_load_out[field],1)).rjust(4),'</span>',end=' ',sep='')
# Total load is 100 - idle, this is a short text output
print('\n',round(100 - cpu_load_out['idle'],1),sep='')
# Print out in red on high load
if cpu_load_out['idle'] < 20: print('#E92F2F')
